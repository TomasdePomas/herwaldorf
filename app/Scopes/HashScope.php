<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;

use Illuminate\Http\Request;

class HashScope implements ScopeInterface{

    public function apply(Builder $builder, Model $model)
    {
        $hash = app('request')->session()->get('hash', app('request')->cookie('hash', hash('adler32',  50 + 1337)));
        $builder->where('hash', '=', $hash);
    }

    /**
     * Remove the scope from the given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     *
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        // Not necessary
    }
}