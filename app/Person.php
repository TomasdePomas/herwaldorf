<?php

namespace App;

use App\Scopes\HashScope;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Person extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'location_id',
        'account_number',
        'metro_number',
        'company_id',
        'description',
        'hash'
    ];

    protected $hidden = [
        'hash'
    ];

    public static function boot()
    {
        static::addGlobalScope(new HashScope());
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
