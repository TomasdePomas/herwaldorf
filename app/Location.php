<?php

namespace App;

use App\Scopes\HashScope;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Location extends Model
{
    public $timestamps = false;

    const TYPE_HOUSE = 0;
    const TYPE_COMPANY = 1;
    const TYPE_BAR = 2;
    const TYPE_PO = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "phone_number",
        "phone_number_humanized",
        "address",
        "type",
        "nearest_station",
        'hash'
    ];

    protected $hidden = [
        'hash'
    ];

    public static function boot()
    {
        static::addGlobalScope(new HashScope());
    }

//    protected $hidden = [
//        "nearest_station"
//    ];
}
