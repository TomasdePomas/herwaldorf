<?php

namespace App\Http\Controllers;

use App\Company;
use App\Hash;
use App\Location;
use App\Person;
use App\Station;
use App\Transaction;
use App\Transportation;
use Faker\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{

    public function peakDataset(Request $request, $seed = false)
    {
        $cookieHash = app('request')->cookie('hash', false);
        $savedHash = false;
        $process = '';
        $faker = Factory::create();

        if ($cookieHash) {
            $process .= 'Hash in cookie, retreiving from database. ';
            $savedHash = Hash::where('hash', $cookieHash)->first();

            if ($savedHash && (! $seed || $seed == $savedHash->seed)) {

                $grain = $savedHash->grain;
                $hash = $savedHash->hash;

                $faker->seed($savedHash->seed);
                $request->session()->put('hash', $savedHash->hash);

            } else {
                $process .= "Unknown hash in cookie ($cookieHash) or new seed ($seed) given, generating new hash. ";

                $grain = Cache::get('grain', 50);
                $seed = $seed ? $seed : 1337;
                $hash = hash('adler32', $grain + $seed);

                $faker->seed($seed);
                $request->session()->put('hash', $hash);

                $savedHash = Hash::where('hash', $hash)->first();
            }
        } else {
            $process .= 'No hash in cookie, generating new hash. ';

            $grain = Cache::pull('grain', 50);
            $hash = hash('adler32', $grain + $seed);

            $request->session()->put('hash', $hash);
            $faker->seed($seed);

            $savedHash = Hash::where('hash', $hash)->first();
        }


        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $handler = Person::first();

        $allBars = Location::where('type', Location::TYPE_BAR)->get()->toArray();
        $meetingBars = [];


        for ($i = 0; $i < DatasetController::meetingAmount; $i++) {
            $index = $faker->numberBetween(0, (count($allBars) - 1));
            array_push($meetingBars, $allBars[$index]);
        }

        dd([
            'waldorf' => Person::skip($num)->take(1)->with('company.location', 'location')->first()->toArray(),
            'handler' => $handler->toArray(),
            'someone' => Person::skip(20)->take(1)->with('company.location', 'location')->first()->toArray(),
            'numbers' => Location::all('phone_number', 'type')->pluck('type', 'phone_number')->toArray(),
            'meetings' => $meetingBars
        ]);
    }
}
