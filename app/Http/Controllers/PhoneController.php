<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\Request;
use App\Location;
use App\Person;
use App\Transportation;
use Faker\Factory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class PhoneController extends Controller
{
    public function call($number, $info = false)
    {
        $hash = DatasetController::retrieveHash();

        $faker = Factory::create();
        $librarian = Person::with('location')->first();
        $handset = Location::where('phone_number', $number)->first();

        if ($handset) {
            $faker->seed($hash->seed + $handset->id);
            $receiver = null;
            $data = [
                "response" => '',
                "audio"    => $handset->id % 7,
            ];
            switch ($handset->type) {
                case Location::TYPE_HOUSE:
                    $person = Person::where('location_id', $handset->id)->first();
                    if ($info && $person->name != $info) {
                        $data['response'] = static::wrongPersonResponse($faker, $info, $person);
                    } else if ($info) {
                        $data['response'] = static::personalResponse($faker, $person);
                    } else {
                        $data['response'] = static::impersonalResponse($faker, $person);
                    }
                    break;
                case Location::TYPE_COMPANY:
                    $company = Company::where('location_id', $handset->id)->first();
                    if ($info) {
                        $data['response'] = static::companyResponseWithDescription($faker, $info, $company);
                    } else {
                        $data['response'] = static::companyResponse($faker, $company);
                    }
                    break;
                case Location::TYPE_BAR:
                    $data['response'] = static::barResponse($faker, $handset);
                    break;
                case Location::TYPE_PO:
                    if ($info) {
                        $data['response'] = static::poCompanyResponseWithDescription($faker, $info, $handset);
                    } else {
                        $data['response'] = static::poCompanyResponse($faker, $handset);
                    }
                    break;
            }

            if ($librarian->location->id == $handset->id) {
                $data['response'] = static::randomLibrarianResponse();
            }


            return Response::json($data, 200);
        }

        return Response::json([], 404);
    }

    private static function barResponse($faker, $location)
    {
        $hash = DatasetController::retrieveHash();
        $faker = Factory::create();
        $faker->seed($hash->seed);

        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $it = Person::skip($num)->take(1)->first();

        $barRelevance = static::checkBarRelevance($location);
        $handler = Person::first();

        if ($barRelevance > 0) {

            if ($barRelevance == 1) {
                $reactions = [
                    "Ey man this is $location->name ..... Yeah I remember that day, I was behind the bar. The man was here for a couple of hours, talked to someone intensely. $it->description. Hope it helps",
                    "Hello? Yes, this is $location->name, yes I do remember someone like that comming in, he sat near the window. Who he spoke to? I don't know. I think $it->description. But I couldn't see too much, it was rather dark",
                    "$location->name, what can I help you with? O yeah, I know who you are talking about, $handler->description, right? He spend all night in deep discussion with someone. $it->description. They left after 2 hours, but they only drank coffee..",
                    "$location->name, $faker->name speaking. O no I didn't work that night but my mother did, and she told me about someone like that coming in. Sat in the corner in the dark. She got a pretty good look at the other one. $it->description",
                    "$faker->name....  $location->name? No that is not me, that is where I work. .. What? Seen someone? Yea I guess I do.. Yea he spoke to someone.. What? A description? $it->description, what are you his wife? I hope he's in trouble."
                ];

                return $reactions[$location->id % (count($reactions) - 1)];
            }

            $reactions = [
                "Ey man this is $location->name ..... Yeah I remember, he visited us a couple of times when I was behind the bar. The man was always here for a couple of hours, always talking to someone intensly. $it->description. Hope it helps ya",
                "Hello? Yes, this is $location->name, yes I do remember someone like that coming in a couple of times, he always sits near the window. Who he speaks to? Now that you mention it, he does come in with someone else. I think $it->description.",
                "$location->name, what can I help you with? O yeah, I know who you are talking about, $handler->description, right? He comes in on Tuesdays, always spends the night in deep discussion. $it->description. They stay for hours but they never order anything but coffee.. Haven't seen him for a while though, is he ok?",
                "$location->name, $faker->name speaking. O no I don't work here but my mother does, and she told me about someone like that coming in. She tells me he is always with some other person. $it->description",
                "$faker->name....  $location->name? No that is not me, that is where I work. .. What? Seen someone? Yea I guess I do.. more than once .. Yea he speaks to someone.. What? A description? $it->description, what are you his wife? I hope he's in trouble."
            ];

            return $reactions[$location->id % (count($reactions) - 1)];
        }

        $reactions = [
            "Ey man this is $location->name ..... Nah dude, doesn't ring a bell",
            "Hello? Yes, this is $location->name. No I'm sorry, I have no idea what you are talking about",
            "$location->name, what can I help you with? $handler->name, never heard of him, he sounds like bad news. Tell him not to come here! O he's dead? Well he probably had it comming",
            "$location->name, $faker->name speaking. What? Now listen to me, this is not lost and found, keep track of your friends yourself!",
            "$faker->name....  $location->name? What? $handler->name? What's it to ya? Are you the cops? Get lost!"
        ];

        return $reactions[$location->id % (count($reactions) - 1)];
    }

    private static function randomLibrarianResponse()
    {
        $reactions = [
            'O it\'s you, well.. Handlers often forward money through a number of small transactions through other accounts to cover their tracks.',
            'The librarian speaks: meetings between handlers and agents often happen in bars. And bartenders often remember mysterious characters',
            'Two people being at the same place once is a coincidence, but two people being on the same place multiple times is a pattern',
            'Who is this? What day is today? Where has the handler been? What is near there? Where are my socks?',
            'Hello? The motherland\'s funds are low, all spies are required to get a day job.',
            '.. uhm ... What do you do when you have secrets to hide? .. You put them in a secret place...',
            'Want to be an agent yourself? It earns a steady $500 a month! .. How do you mean \'so little\'?'
        ];

        return $reactions[random_int(0, count($reactions) - 1)];
    }

    private static function companyResponse($faker, $company)
    {
        $faker->seed();
        $worker = $faker->firstName('male');
        $reactions = [
            "Hello? This is $company->name",
            "$company->name, What can I help you with?",
            "Good day, you have reached $company->name.",
            "Yeah? This is $company->name. .. Hey, $worker! Get back to work!",
            "$company->name, $worker speaking, what can I do for you today?"
        ];

        return $reactions[$company->id % (count($reactions) - 1)];
    }

    private static function companyResponseWithDescription($faker, $info, $company)
    {
        $people = Person::where('description', 'like', "%$info%")
            ->where('company_id', $company->id)
            ->get(['id'])
            ->toArray();
        $faker->seed();

        if (count($people) > 1) {
            $worker = $faker->firstName('male');
            $reactions = [
                "Hello? Yes, this is $company->name. .. ... No, I am sorry, That sounds a little bit too vague.",
                "$company->name, What can I help you with? .. .. That is a very popular look here, I am going to need a little more than that.",
                "Good day, you have reached $company->name. .. .. Yeah, there is more than one person who looks like that who works here.",
                "Yeah? This is $company->name. .. That could be anybody! Hey, $worker, listen to this guy! Ha Ha Ha!",
                "$company->name, $worker speaking, what can I do for you today? Hmm... No I am sorry, that is too generic to help you further."
            ];

            return $reactions[$company->id % (count($reactions) - 1)];

        } else if (count($people) == 1 && static::checkCompanyRelevance($company)) {

            $worker = $faker->firstName('male');
            $hash = DatasetController::retrieveHash();
            $faker = Factory::create();
            $faker->seed($hash->seed);

            $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
            $it = Person::skip($num)->take(1)->with('location')->first();

            if ($it->id == $people[0]['id'])
                $reactions = [
                    "Hello? Yes, this is $company->name. .. ... Yes, that sounds like $it->name. I think the adress is " . $it->location->address,
                    "$company->name, What can I help you with? .. .. That is a very popular look here, I am going to need a little more than that.",
                    "Good day, you have reached $company->name. .. .. Yeah, I know $it->name, " . $it->location->address . " there you'll find who you are looking for",
                    "Yeah? This is $company->name. .. Hey, $worker! Someone is looking for $it->name, you know where they live? Ah, " . $it->location->address . ", that's where.",
                    "$company->name, $worker speaking, what can I do for you today? Hmmm that could be $it->name. Lets see.. The address registered in our administration is " . $it->location->address
                ];

            return $reactions[$company->id % (count($reactions) - 1)];
        }


        $worker = $faker->firstName('male');
        $reactions = [
            "Hello? Yes, this is $company->name. .. ... No, I am sorry, I don't know anyone who fits that description",
            "$company->name, What can I help you with? .. .. No.. No I don't think someone like that works here.",
            "Good day, you have reached $company->name. No, that does not sound like one of my employees",
            "Yeah? This is $company->name. .. Nah, I know nobody like that.. Hang on maybe $worker knows.. $worker!! $worker!!! GET IN HERE!! .. .. .. No, $worker don't know either.",
            "$company->name, $worker speaking, what can I do for you today? Hmm... No I am sorry, that does not ring a bell."
        ];

        return $reactions[$company->id % (count($reactions) - 1)];

    }

    private static function poCompanyResponse($faker, $location)
    {
        if (Session::get('PO_Called', 0) == 3) {
            return "You have reached $location->name, how can I be the of service?";
        }
        return "You have reached $location->name, how can I be of service?";
    }

    private static function poCompanyResponseWithDescription($faker, $info, $location)
    {
        $hash = DatasetController::retrieveHash();
        $faker = Factory::create();
        $faker->seed($hash->seed);

        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $it = Person::skip($num)->take(1)->first();
        $handler = Person::first();

        $number = $handler->id . $it->id . $location->id;

        if (is_numeric($info)) {
            if ($info == $number) {
                if (Session::get('PO_Called', 0) == 1) {

                    Session::put('PO_Called', 2);

                    return "You have reached " . substr($location->name, 0, 8) . "... Oh it's you again.. .. No no please don't tell my boss!! Ok, I'll open the box for you, do you promise you won't tell my boss? .... ..... ....... <click> .... ... Ok, I've got it, but it is just some junk. Some letters from " . $it->company->name . " and some phone contract papers.. The number? " . $it->location->phone_number_humanized . " So now you leave me alone right? Please..";
                }
                if (Session::get('PO_Called', 0) == 2) {
                    Session::put('PO_Called', 3);

                    return "You have reached $location->name, how can I be the of service? The previous receptionist? He got fired, big time. I think he might be in jail. Thanks for tipping us off by the way.";
                }
                if (Session::get('PO_Called', 0) == 3) {
                    return "You have reached $location->name, how can I be the of service?";
                }

                Session::put('PO_Called', 1);
                return "You have reached $location->name, how can I help? Yeah, I know who comes here for $number, $it->description, right? I can see in our records that number is registered to " . $it->location->address . ". .. O wait, is this what they where talking about in that security seminar...? Please don't tell my boss..";
            }

            return "You have reached $location->name. $info? No I think that is not one of our boxes.";
        } else {
            $people = Person::where('description', 'like', "%$info%")
                ->get(['id'])
                ->toArray();

            if (count($people) > 1) {
                return "You have reached $location->name, what can I do for you today? .. No I am sorry, we have loads of people come and go every day, I am going to need more than that.";
            } else if (count($people) == 1) {
                return "You have reached $location->name, what can I do for you today? .. Yes, yes I think I know who that is. Comes in to empty box $number right..? O .. wait.. I was not supposed to tell you that! Forget that please! ... Hello?..";
            }

            return "You have reached $location->name, how can I help? No, that does not sound like someone I have seen.";
        }
    }


    private static function impersonalResponse($faker, $person)
    {
        $reactions = [
            "Hello?",
            "What?",
            "Who is this?",
            "...",
            "Good day, who am I speaking?",
            "Who's on the line?",
            "Who you gonna call!? .... Me!",
        ];

        return $reactions[$person->id % (count($reactions) - 1)];
    }

    private static function personalResponse($faker, $person)
    {
        if (static::checkPerson($person)) {
            return "Hello? .. ..You found me! победа и тунец бутерброды за Родину!! Все ваши базы принадлежат нам!!!";
        }

        $reactions = [
            "This is $person->name, who is this?",
            "$person->name here",
            "Hello, $person->name speaking. Who is this?",
            ".. $person->name ..",
            "Good day, $person->name Residence, who am I speaking?",
            "$person->name, Who's on the line?",
            "Who you gonna call!? .... $person->name!! Who is this?",
        ];

        return $reactions[$person->id % (count($reactions) - 1)];

    }

    private static function wrongPersonResponse($faker, $name, $person)
    {
        $reactions = [
            "Hello? You must have dailed for someone else.",
            "What? No, not $name.",
            "Who is this? $name? Nope, not here",
            "... Wrong number ..",
            "Good day, No I am sorry, there is no $name here",
            "You have me mistaken, I am not $name",
            "Who you gonna call!? .... Someone else, because $name does not live here!",
        ];
        return $reactions[$person->id % (count($reactions) - 1)];
    }


    private static function checkPerson($person)
    {
        $hash = DatasetController::retrieveHash();
        $faker = Factory::create();
        $faker->seed($hash->seed);

        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $it = Person::skip($num)->take(1)->first();

        return $it->id == $person->id;
    }

    private static function checkBarRelevance($location)
    {
        $hash = DatasetController::retrieveHash();
        $faker = Factory::create();
        $faker->seed($hash->seed);

        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $it = Person::skip($num)->take(1)->with('company')->first();

        $handler = Person::first();
        $allBars = Location::where('type', Location::TYPE_BAR)->select('id')->get()->toArray();
        $meetingBars = [];


        for ($i = 0; $i < DatasetController::meetingAmount; $i++) {
            $index = $faker->numberBetween(0, (count($allBars) - 1));
            array_push($meetingBars, $allBars[$index]['id']);
        }


        $counted = array_count_values($meetingBars);

        return key_exists($location->id, $counted) ? $counted[$location->id] : 0;
    }

    private static function checkCompanyRelevance($company)
    {
        $hash = DatasetController::retrieveHash();
        $faker = Factory::create();
        $faker->seed($hash->seed);

        $num = $faker->numberBetween(0, (DatasetController::peopleAmount - 2));
        $it = Person::skip($num)->take(1)->with('company')->first();

        return $it->company->id == $company->id;

    }
}
