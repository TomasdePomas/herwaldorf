<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Support\Facades\Response;

class PhoneBookController extends Controller
{
    public function findByName($name)
    {
        $location = Location::where('name', "like", "%".$name."%")->get();

        if ($location) {
            return Response::json($location, 200);
        }
        return Response::json([], 404);
    }

    public function findByAddress($address)
    {
        $location = Location::where('address', "like", "%".$address."%")->get();

        if ($location) {
            return Response::json($location, 200);
        }
        return Response::json([], 404);
    }
}
