<?php

namespace App\Http\Controllers;

use App\Company;
use App\Hash;
use App\Location;
use App\Person;
use App\Station;
use App\Transaction;
use App\Transportation;
use Exception;
use Faker\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class DatasetController extends Controller
{
    const companyAmount = 50;
    const stationAmount = 20;
    const peopleAmount = 800;
    const barAmount = 15;
    const bulkStepAmount = 5;
    const meetingAmount = 6;
    const transPerPerson = 5;

    public static function retrieveHash()
    {
        $cookieHash = app('request')->cookie('hash', false);
        if ($cookieHash) {
            $savedHash = Hash::where('hash', $cookieHash)->first();
        } else {
            $grain = 50;
            $seed = 2017;
            $hash = hash('adler32', $grain + $seed);

            $savedHash = Hash::where('hash', $hash)->first();
        }
        if (! $savedHash || $savedHash->step < 7) {
            throw new Exception('This hash is not properly generated');
        }

        return $savedHash;

    }

    public function generateData(Request $request)
    {
        $seed = 2017;
        $cookieHash = app('request')->cookie('hash', false);
        $savedHash = false;
        $faker = Factory::create();
        $responseData = [];

        if ($cookieHash) {
            $savedHash = Hash::where('hash', $cookieHash)->first();

            if ($savedHash && (! $seed || $seed == $savedHash->seed)) {

                $grain = (int) $savedHash->grain;
                $seed = (int) $savedHash->seed;
                $hash = $savedHash->hash;

                $faker->seed($seed);
                $request->session()->put('hash', $savedHash->hash);

            } else {

                $grain = 50;
                $seed = $seed ? (int) $seed : 1337;
                $hash = hash('adler32', $grain + $seed);

                $faker->seed($seed);
                $request->session()->put('hash', $hash);

                $savedHash = Hash::where('hash', $hash)->first();
            }
        } else {

            $seed = (int) $seed;
            $grain = 50;
            $hash = hash('adler32', $grain + $seed);

            $request->session()->put('hash', $hash);
            $faker->seed($seed);

            $savedHash = Hash::where('hash', $hash)->first();
        }

        $step = $savedHash ? $savedHash->step + 1 : 1;

        if (! $savedHash || $step <= 7) {


            switch ($step) {
                case 1:
                    static::generateStations($faker, $hash);
                    break;
                case 2:
                    static::generateCompanies($faker, $hash);
                    break;
                case 3:
                    static::generatePeople($faker, $hash);
                    break;
                case 4:
                    static::generateTransactions($faker, $hash);
                    break;
                case 5:
                    static::generateBarsAndBoxes($faker, $hash);
                    break;
                case 6:
                    static::generateMeetingsAndTransactions($faker, $hash);
                    break;
                case 7:
                    static::generateExtraDistortion($faker, $hash);
                    break;
                default:
                    echo "Unknown step: $step";
                    break;
            }

            if ($savedHash) {
                $savedHash->step = $step;
                $savedHash->save();
            } else {
                $savedHash = Hash::create([
                    'hash'  => $hash,
                    'grain' => $grain,
                    'seed'  => $seed,
                    'step'  => 1,
                ]);
            }

        } else {
            $handler = Person::first();

            $barStations = Location::where('type', Location::TYPE_BAR)->select('nearest_station')->get()->pluck('nearest_station')->toArray();

            $meetings = Transportation::where('metro_number', $handler->metro_number)
                ->whereIn('to_id', $barStations)
                ->get()
                ->pluck('to_id', 'date')
                ->toArray();

            $allBars = Location::where('type', Location::TYPE_BAR)->get()->toArray();
            $meetingList = [];

            $bust = $faker->numberBetween(0, count($allBars)); // Not used

            for ($i = 0; $i < DatasetController::meetingAmount; $i++) {
                $index = $faker->numberBetween(0, (count($allBars) - 1));
                $bar = $allBars[$index];
                $meeting = array_search($bar['nearest_station'], $meetings);

                unset($meetings[$meeting]);

                $words = explode(" ", $bar['name']);
                $barcronym = "";

                foreach ($words as $w) {
                    $barcronym .= $w[0];
                }

                array_push($meetingList, [
                    'code' => $barcronym,
                    'date' => $meeting
                ]);
            }

            $responseData['start_data'] = [
                'meetings'  => $meetingList,
                'handler'   => [
                    'name'           => $handler->name,
                    'description'    => $handler->description,
                    'metro_number'   => $handler->metro_number,
                    'account_number' => $handler->account_number,
                    'station'        => Station::find($handler->location->nearest_station)->name
                ],
                'librarian' => $handler->location->phone_number_humanized
            ];
        }

        $responseData['step'] = $savedHash->step;
        $response = new Response($responseData);
        $response->withCookie(cookie()->forever('hash', $hash));

        return $response;
    }

    private static function generateStations($faker, $hash)
    {

        for ($i = 0; $i < static::stationAmount; $i++) {
            Station::create([
                'name' => $faker->cityPrefix . " " . $faker->streetSuffix,
                'hash' => $hash
            ]);
        }
    }

    private static function generateCompanies($faker, $hash)
    {

        for ($i = 0; $i < static::companyAmount; $i++) {
            $companyName = $faker->company;
            $companyPhone = substr($faker->phoneNumber, 0, 12);

            $num = $faker->numberBetween(0, (static::stationAmount - 2));
            $nearestStation = Station::skip($num)->take(1)->get()->first();

            $companyLocation = Location::create([
                "name"                   => $companyName,
                "phone_number_humanized" => $companyPhone,
                "phone_number"           => preg_replace('/\D+/', '', $companyPhone),
                "address"                => $faker->streetAddress,
                "type"                   => Location::TYPE_COMPANY,
                "nearest_station"        => $nearestStation->id,
                'hash'                   => $hash
            ]);

            Company::create([
                'name'           => $companyName,
                'location_id'    => $companyLocation->id,
                'account_number' => $faker->creditCardNumber,
                'hash'           => $hash
            ]);
        }
    }

    private static function generatePeople($faker, $hash)
    {
        for ($i = 0; $i < static::peopleAmount; $i++) {
            $userPhone = substr($faker->phoneNumber, 0, 12);

            $num = $faker->numberBetween(0, (static::stationAmount - 2));
            $nearestStation = Station::skip($num)->take(1)->get()->first();

            $num = $faker->numberBetween(0, (static::companyAmount - 2));
            $workPlace = Company::skip($num)->take(1)->get()->first();

            $userLocation = Location::create([
                "phone_number_humanized" => $userPhone,
                "phone_number"           => preg_replace('/\D+/', '', $userPhone),
                "address"                => $faker->streetAddress,
                "type"                   => Location::TYPE_HOUSE,
                "nearest_station"        => $nearestStation->id,
                'hash'                   => $hash
            ]);

            $gender = $i == 0 ? 'male' : $faker->randomElement(['male', 'female']);
            $person = Person::create([
                'name'           => $faker->name($gender),
                'location_id'    => $userLocation->id,
                'account_number' => $faker->creditCardNumber,
                'metro_number'   => strtoupper($faker->bothify('MET-##???#-??#?-??-#')),
                'company_id'     => $workPlace->id,
                'description'    => static::generateDescription($gender, $faker),
                'hash'           => $hash
            ]);
        }
    }

    private static function generateTransactions($faker, $hash)
    {
        foreach (Person::with('company')->get() as $person) {
            $rent = $faker->numberBetween(2, 20) * 10000;
            $wages = $faker->randomFloat(2, 80000, 400000);

            for ($i = 0; $i < static::transPerPerson; $i++) {

                switch ($faker->numberBetween(0, 1)) {
                    case 0:
                        Transaction::create([
                            "amount"       => $rent,
                            "from_account" => $person->account_number,
                            "description"  => "Rent payment ".$person->location->address,
                            "hash"         => $hash
                        ]);
                        break;
                    case 1:
                        Transaction::create([
                            "amount"       => $wages,
                            "to_account"   => $person->account_number,
                            "from_account" => $person->company->account_number,
                            "description"  => "Salary payment " . $person->company->name,
                            "hash"         => $hash
                        ]);
                        break;
                }
            };

            Transaction::create([
                "amount"       => "755",
                "from_account" => $person->account_number,
                "description"  => "MetroCard",
                "hash"         => $hash
            ]);
        }
    }

    private function generateBarsAndBoxes($faker, $hash)
    {

        for ($i = 0; $i < static::barAmount; $i++) {
            $barPhone = substr($faker->phoneNumber, 0, 12);

            $num = $faker->numberBetween(0, (static::stationAmount - 2));
            $nearestStation = Station::skip($num)->take(1)->get()->first();

            Location::create([
                "name"                   => static::generateBarName($nearestStation, $faker),
                "phone_number_humanized" => $barPhone,
                "phone_number"           => preg_replace('/\D+/', '', $barPhone),
                "address"                => $faker->streetAddress,
                "type"                   => Location::TYPE_BAR,
                "nearest_station"        => $nearestStation->id,
                'hash'                   => $hash
            ]);
        }

        $poPhone = substr($faker->phoneNumber, 0, 12);
        $num = $faker->numberBetween(0, (static::stationAmount - 2));
        $nearestStation = Station::skip($num)->take(1)->get()->first();

        Location::create([
            "name"                   => $faker->name . " PO. Boxes",
            "phone_number_humanized" => $poPhone,
            "phone_number"           => preg_replace('/\D+/', '', $poPhone),
            "address"                => $faker->streetAddress,
            "type"                   => Location::TYPE_PO,
            "nearest_station"        => $nearestStation->id,
            'hash'                   => $hash
        ]);

    }

    private function generateMeetingsAndTransactions($faker, $hash)
    {
        $num = $faker->numberBetween(0, (static::peopleAmount - 2));
        $waldo = Person::skip($num)->take(1)->get()->first();
        $handler = Person::first();

        $allBars = Location::where('type', Location::TYPE_BAR)->get()->toArray();
        $meetingBars = [];


        for ($i = 0; $i < static::meetingAmount; $i++) {
            $index = $faker->numberBetween(0, (count($allBars) - 1 ));
            array_push($meetingBars, $allBars[$index]);
        }

        foreach ($meetingBars as $randomBar) {
            $date = $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days");

            Transportation::create([
                'from_id'      => $waldo->location->nearest_station,
                'to_id'        => $randomBar['nearest_station'],
                'date'         => $date,
                'metro_number' => $waldo->metro_number,
                'hash'         => $hash
            ]);
            Transportation::create([
                'from_id'      => $handler->location->nearest_station,
                'to_id'        => $randomBar['nearest_station'],
                'date'         => $date,
                'metro_number' => $handler->metro_number,
                'hash'         => $hash
            ]);
        }

        $poBox = Location::where('type', Location::TYPE_PO)->first();

        for ($i = 0; $i < floor(static::meetingAmount); $i++) {
            switch ($faker->numberBetween(0, 6)) {
                case 0:
                case 1:
                case 2:
                    Transportation::create([
                        'from_id'      => $waldo->company->location->nearest_station,
                        'to_id'        => $waldo->location->nearest_station,
                        'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                        'metro_number' => $waldo->metro_number,
                        'hash'         => $hash
                    ]);
                    break;
                case 3:
                case 4:
                case 5:
                    Transportation::create([
                        'from_id'      => $waldo->location->nearest_station,
                        'to_id'        => $waldo->company->location->nearest_station,
                        'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                        'metro_number' => $waldo->metro_number,
                        'hash'         => $hash
                    ]);
                    break;
                case 6:
                    Transportation::create([
                        'from_id'      => $waldo->location->nearest_station,
                        'to_id'        => $poBox->nearest_station,
                        'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                        'metro_number' => $waldo->metro_number,
                        'hash'         => $hash
                    ]);
                    Transaction::create([
                        "amount"       => $faker->numberBetween(1000, 15000),
                        "from_account" => $waldo->account_number,
                        "description"  => "POBox nr.".$handler->id.$waldo->id.$poBox->id,
                        "hash"         => $hash
                    ]);
                    break;
            }
        }

        $total = 50000;
        $send = 0;
        $handler = Person::first();

        for ($i = 0; $i < (static::transPerPerson - 1); $i++) {

            $amount = $faker->numberBetween(1, $total - $send - 100);
            $previousLink = $handler->account_number;

            for ($j = 0; $j < (static::bulkStepAmount - 1); $j++) {
                $num = $faker->numberBetween(1, (static::peopleAmount - 2));
                $randomReceiver = Person::skip($num)->take(1)->get()->first();

                Transaction::create([
                    "amount"       => $amount,
                    "from_account" => $previousLink,
                    "to_account"   => $randomReceiver->account_number,
                    "description"  => $faker->isbn10,
                    "hash"         => $hash
                ]);

                $previousLink = $randomReceiver->account_number;
            }

            Transaction::create([
                "amount"       => $amount,
                "from_account" => $previousLink,
                "to_account"   => $waldo->account_number,
                "description"  => $faker->isbn10,
                "hash"         => $hash
            ]);

            $send += $amount;
        }

        $amount = $total - $send;
        $previousLink = $handler->account_number;

        for ($j = 0; $j < (static::bulkStepAmount - 1); $j++) {
            $num = $faker->numberBetween((static::peopleAmount - 2), 1);
            $randomReceiver = Person::skip($num)->take(1)->get()->first();

            Transaction::create([
                "amount"       => $amount,
                "from_account" => $previousLink,
                "to_account"   => $randomReceiver->account_number,
                "description"  => $faker->isbn10,
                "hash"         => $hash
            ]);

            $previousLink = $randomReceiver->account_number;
        }

        Transaction::create([
            "amount"       => $amount,
            "from_account" => $previousLink,
            "to_account"   => $waldo->account_number,
            "description"  => $faker->isbn10,
            "hash"         => $hash
        ]);
    }

    private function generateExtraDistortion($faker, $hash)
    {
        $poBox = Location::where('type', Location::TYPE_PO)->first();
        $handler = Person::first();

        foreach (Person::with('company.location', 'location')->get() as $person) {
            if ($person->id != $handler->id) {

                for ($i = 0; $i < (static::meetingAmount * 2); $i++) {
                    $has_poBox = $faker->boolean(20);

                    switch ($faker->numberBetween(0, 12)) {
                        case 0:
                        case 1:
                        case 2:
                            Transportation::create([
                                'from_id'      => $person->company->location->nearest_station,
                                'to_id'        => $person->location->nearest_station,
                                'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                                'metro_number' => $person->metro_number,
                                'hash'         => $hash
                            ]);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            Transportation::create([
                                'from_id'      => $person->location->nearest_station,
                                'to_id'        => $person->company->location->nearest_station,
                                'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                                'metro_number' => $person->metro_number,
                                'hash'         => $hash
                            ]);
                            break;
                        case 6:
                            if ($has_poBox) {
                                Transportation::create([
                                    'from_id'      => $person->location->nearest_station,
                                    'to_id'        => $poBox->nearest_station,
                                    'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                                    'metro_number' => $person->metro_number,
                                    'hash'         => $hash
                                ]);
                                break;
                            }
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            $randomBar = Location::where('type', Location::TYPE_BAR)->get()->random(1);
                            Transportation::create([
                                'from_id'      => $person->location->nearest_station,
                                'to_id'        => $randomBar->nearest_station,
                                'date'         => $faker->dateTimeInInterval('-34 years', "+ " . (static::meetingAmount * 4) . " days"),
                                'metro_number' => $person->metro_number,
                                'hash'         => $hash
                            ]);
                            break;

                    }
                }

                for ($i = 0; $i < (static::transPerPerson - 1); $i++) {
                    $amount = $faker->numberBetween(100, 100000);
                    $num = $faker->numberBetween((static::peopleAmount - 2), 1);
                    $randomReceiver = Person::skip($num)->take(1)->get()->first();

                    Transaction::create([
                        "amount"       => $amount,
                        "from_account" => $person->account_number,
                        "to_account"   => $randomReceiver->account_number,
                        "description"  => $faker->isbn10,
                        "hash"         => $hash
                    ]);
                }
            }
        }
    }

    private static function generateDescription($gender, $faker)
    {

        $characteristics = [
            'eyes'       => [
                'blue',
                'light blue',
                'grey',
                'green',
                'brown',
                'brownish green',
                'amber',
                'dark brown',
                'hazel',
            ],
            'hair_color' => [
                'dark',
                'blonde',
                'brown',
                'raven',
                'almond',
                'red',
                'grey',
                'auburn',
                'ashen'
            ],
            'hair_type'  => [
                'curly',
                'ragged',
                'straight',
                'short',
                'short \'n spicy',
                'wavy',
                'afro',
                'bob cut',
                'comb over',
                'cornrows',
                'dreadlocks',
                'a ponytail',
                'a mullet',
                'a long beard',
                'a moustache',
            ],
            'body'       => [
                'large',
                'skinny',
                'fat',
                'husky',
                'tall',
                'short',
                'chubby',
                'pale skinned',
                'dark skinned',
                'black',
                'asian',
                'mexican',
                'wearing glasses',
                'wearing a hat',
                'wearing a trenchcoat',
            ],
            'age'        => [
                'young',
                'a teenager',
                'old',
                'not old',
                'older',
                'middle age',
                'in [OWN] twenties',
                'in [OWN] thirties',
                'in [OWN] forties',
                'in [OWN] fifties',
                'in [OWN] sixties',
                'in [OWN] seventies',
                'not older than 10',
                'between 10 and 15',
                'between 15 and 20',
                'between 20 and 25',
                'between 25 and 35',
                'between 35 and 45',
                'between 45 and 60',
                'between 60 and 80',
                'not younger than 90',
            ]
        ];

        $proNoun = $gender == 'male' ? 'his' : 'her';
        $noun = $gender == 'male' ? 'he' : 'she';
        $gender = $gender == 'male' ? 'man' : 'woman';

        $descriptionParts = [];

        $descriptionParts[] = $noun . ' was ' . $faker->randomElement($characteristics['body']);
        $descriptionParts[] = $noun . ' had ' . $faker->randomElement($characteristics['eyes']) . ' eyes';
        $descriptionParts[] = $noun . ' had ' . $faker->randomElement($characteristics['hair_type']) . ', ' . $faker->randomElement($characteristics['hair_color']) . ' hair';
        $descriptionParts[] = $faker->randomElement($characteristics['age']);


        $description = join(', ', $faker->shuffle($descriptionParts)) . '.';

        $description = str_replace('[OWN]', $proNoun, $description);
        $description = str_replace('[IT]', $noun, $description);

        return ucfirst($description);
    }

    private
    static function generateBarName($nearestStation, $faker)
    {
        switch ($faker->numberBetween(0, 10)) {
            case 0:
                return $faker->cityPrefix . " " . $faker->numberBetween(0, 1000);
            case 1:
                return $faker->title . " " . $faker->firstName;
            case 2:
                return $faker->title . " " . $faker->firstName . " " . $faker->companySuffix;
            case 3:
                return $nearestStation->name . " Tavern";
            case 4:
                return $nearestStation->name . " Inn";
            case 5:
                return $faker->cityPrefix . " Restaurant";
            case 6:
                return $faker->state . "'s Bar";
            case 7:
                return $faker->streetName . " " . $faker->century;
            case 8:
                return $faker->streetSuffix . " " . $faker->cityPrefix;
            case 9:
                return $faker->colorName . " " . $faker->streetSuffix . " " . $faker->companySuffix;
            case 10:
                return $faker->firstName . "'s Steakhouse";
        }

        return $nearestStation->name . " Restaurant and bar";
    }
}
