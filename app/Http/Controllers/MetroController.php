<?php

namespace App\Http\Controllers;

use App\Location;
use App\Station;
use App\Transportation;
use Faker\Factory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class MetroController extends Controller
{
    public function verifyCode()
    {
        if ($this->requestIsValid()) {
            return Response::json([], 200);
        }

        return Response::json([], 401);
    }

    public function getOverview()
    {
        if ($this->requestIsValid()) {
            return Response::json(
                Station::all()->toArray()
                , 200);
        }

        return Response::json([], 401);
    }

    public function findByStation($stationName)
    {
        if ($this->requestIsValid()) {

            $station = Station::where('name', $stationName)->first();
            if ($station) {
                return Response::json(
                    Transportation::where('to_id', $station->id)
                                  ->with('origin', 'destination')
                                  ->orderBy('date', 'desc')
                                  ->get()
                                  ->toArray()
                    , 200);
            }

            return Response::json([], 404);
        }

        return Response::json([], 401);
    }

    public function findByNumber($metro_number)
    {
        if ($this->requestIsValid()) {
            $transportations = Transportation::where('metro_number', $metro_number)
                ->with('origin', 'destination')
                ->orderBy('date', 'desc')
                ->get()
                ->toArray();
            if ($transportations) {
                return Response::json($transportations, 200);
            }

            return Response::json([], 404);
        }

        return Response::json([], 401);
    }

    public function findNearby($stationName)
    {
        if ($this->requestIsValid()) {
            $station = Station::where('name', $stationName)->first();
            if ($station) {
                return Response::json(
                    Location::where('nearest_station', $station->id)
                        ->where('type', '!=', Location::TYPE_HOUSE)
                        ->get()
                        ->toArray()
                    , 200);
            }

            return Response::json([], 404);
        }

        return Response::json([], 401);
    }

    public function findByAddress($address)
    {
        if ($this->requestIsValid()) {
            $location = Location::where('address', $address)->first();
            if ($location) {
                return Response::json(
                    Station::find($location->nearest_station)
                        ->toArray()
                    , 200);
            } else {
                $location = Location::where('name', $address)->first();
                if ($location) {
                    return Response::json(
                        Station::find($location->nearest_station)
                            ->toArray()
                        , 200);
                }
            }

            return Response::json([], 404);
        }

        return Response::json([], 401);
    }


    private function requestIsValid()
    {
        return true;
    }

}
