<?php

namespace App\Http\Controllers;

use App\Transaction;
use Faker\Factory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class BankController extends Controller
{
    public function verifyCode(){
        if($this->requestIsValid()){
            return Response::json([], 200);
        }
        return Response::json([], 401);
    }

    public function findByNumber($account_number)
    {
        if($this->requestIsValid()){
            $transactions = Transaction::where('from_account', $account_number)
                                       ->orWhere('to_account', $account_number)
                                       ->get();
            if($transactions){
                return Response::json($transactions, 200);
            }
            return Response::json([], 404);
        }
        return Response::json([], 401);
    }

    private function requestIsValid(){
        $code = Input::get('code');
        $hash = DatasetController::retrieveHash();

        $faker = Factory::create();
        $faker->seed($hash->seed);
        $bankcode = $faker->numberBetween(20, 50) * 10 + 2;

        return $code && $code == $bankcode;
    }

}
