<?php

use Illuminate\Routing\Router;

Route::get("/", function(){ return View('terminal'); });

Route::get("peak/{hash?}", 'DataController@peakDataset');

Route::get("generate_data/{hash?}", 'DatasetController@generateData');

Route::get("phone/call/{number}/{name?}", 'PhoneController@call');

Route::get("metro/verify", 'MetroController@verifyCode');

Route::get("metro/overview", 'MetroController@getOverview');

Route::get("metro/by_station/{station}", 'MetroController@findByStation');

Route::get("metro/by_number/{number}", 'MetroController@findByNumber');

Route::get("metro/whats_near/{station}", 'MetroController@findNearby');

Route::get("metro/by_address/{address}", 'MetroController@findByAddress');

Route::get("bank/verify", 'BankController@verifyCode');

Route::get("bank/by_number/{account_number}", 'BankController@findByNumber');

Route::get("phonebook/by_name/{name}", 'PhoneBookController@findByName');

Route::get("phonebook/by_address/{address}", 'PhoneBookController@findByAddress');
