<?php

namespace App;

use App\Scopes\HashScope;
use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_id',
        'to_id',
        'date',
        'metro_number',
        'hash'
    ];

    protected $hidden = [
        'hash'
    ];

    public static function boot()
    {
        static::addGlobalScope(new HashScope());
    }

    public function origin()
    {
        return $this->hasOne('App\Station', 'id', 'from_id');
    }

    public function destination()
    {
        return $this->hasOne('App\Station', 'id', 'to_id');
    }

    public function traveler()
    {
        return $this->hasOne('App\Person', 'metro_number', 'metro_number');
    }
}
