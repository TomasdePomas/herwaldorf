<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hash extends Model
{
    public $timestamps = false;

    protected $fillable = [
        "hash",
        "step",
        "seed",
        "grain"
    ];
}
