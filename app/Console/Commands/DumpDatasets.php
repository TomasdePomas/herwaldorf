<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DumpDatasets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dump-datasets {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::table('companies')->delete();
            DB::table('locations')->delete();
            DB::table('stations')->delete();
            DB::table('transactions')->delete();
            DB::table('transportations')->delete();
            DB::table('people')->delete();


            if ($this->option('all')) {
                DB::table('hashes')->delete();
            } else {
                DB::table('hashes')->update(array('step' => 0));
            }

        } catch (\Error $e) {
            die("Unable to delete datasets\n");
        }

        if ($this->option('all')) {
            echo "All datasets and hashes have been purged\n";

        } else {
            echo "All datasets have been purged\n";
        }
    }
}
