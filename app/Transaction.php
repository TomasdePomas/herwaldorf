<?php

namespace App;

use App\Scopes\HashScope;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Transaction extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "amount",
        "from_account",
        "to_account",
        "description",
        'hash'
    ];

    protected $hidden = [
        'hash'
    ];

    public static function boot()
    {
        static::addGlobalScope(new HashScope());
    }


    public function privateSender()
    {
        return $this->belongsTo('App\Person', 'from_account', 'account_number');
    }

    public function privateReceiver()
    {
        return $this->belongsTo('App\Person', 'to_account', 'account_number');
    }

    public function corporateSender()
    {
        return $this->belongsTo('App\Company', 'from_account', 'account_number');
    }

    public function corporateReceiver()
    {
        return $this->belongsTo('App\Company', 'to_account', 'account_number');
    }
}
