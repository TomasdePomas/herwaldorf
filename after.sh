#!/usr/bin/env bash

cd /home/vagrant/laravel
composer install
php artisan migrate
php artisan db:seed
npm install
npm install node-sass
npm rebuild node-sass
/home/vagrant/laravel/node_modules/gulp/bin/gulp.js
sudo service apache2 restart