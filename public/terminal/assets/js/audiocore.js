/**
 * Created by tomas on 16-1-16.
 */



AudioCore = {
    audioContext: new AudioContext(),
    oscillators: [],

    createAudioElement: function (url, loop) {
        var audioElement = document.createElement("audio");

        audioElement.autoplay = false;
        audioElement.crossOrigin = "anonymous";

        if (loop) {
            audioElement.loop = true;
        } else {
            audioElement.loop = false;
        }

        var typeStr = "audio/" + url.split(".").pop();

        if (audioElement.canPlayType === undefined ||
            audioElement.canPlayType(typeStr).replace(/no/, "")) {

            var sourceElement = document.createElement("source");
            sourceElement.type = typeStr;
            sourceElement.src = url;
            audioElement.appendChild(sourceElement);
        }

        return audioElement;
    },
    initAudio: function (url, loop) {
        var audioElement = this.createAudioElement(url, loop);

        if (audioElement) {
            var source = this.audioContext.createMediaElementSource(audioElement);
            var analyser = this.audioContext.createAnalyser();
            source.connect(analyser);

            analyser.connect(this.audioContext.destination);
        }
        return audioElement;
    },

    initialize: function(){

        this.buttonclick = this.initAudio('assets/audio/click.mp3');

        this.bootup = this.initAudio('assets/audio/bootup.mp3');
        this.welcome = this.initAudio('assets/audio/welcome.mp3');
        this.bootdown = this.initAudio('assets/audio/bootdown.mp3');

        this.idle = this.initAudio('assets/audio/idle-long.ogg', true);
        this.crackle = this.initAudio('assets/audio/crackle1.mp3', true);

        this.disk = this.initAudio('assets/audio/crackle2.mp3');
        this.beebs = {
            start : this.initAudio('assets/audio/beeb1.mp3'),
            error : this.initAudio('assets/audio/beeb2.mp3'),
            default : this.initAudio('assets/audio/beeb3.mp3')
        };

        this.babble = [
            this.initAudio('assets/audio/babble1.mp3'),
            this.initAudio('assets/audio/babble2.mp3'),
            this.initAudio('assets/audio/babble3.mp3'),
            this.initAudio('assets/audio/babble4.mp3'),
            this.initAudio('assets/audio/babble5.mp3'),
            this.initAudio('assets/audio/babble6.mp3'),
            this.initAudio('assets/audio/babble7.mp3'),
            this.initAudio('assets/audio/babble8.mp3')
        ];
    },


    clickButton: function(){
        this.buttonclick.load();
        this.buttonclick.play();
    },

    bootupAudio: function(){
        var that = this;
        this.bootup.onended = function(){
            that.idle.play();
        };
        this.bootup.play();
    },
    playWelcome: function(welcome){
        this.welcome.load();
        this.welcome.play();
    },

    processing: function(timeout){
        var that = this,
            subTimeoutOn = timeout < 1000 ?
                timeout : timeout > 2200 ?
                    Math.round(Math.random() * 2000) + 200 :
                    Math.round(Math.random() * timeout) + 200,

            subTimeoutOff = timeout - subTimeoutOn > 0  ? Math.round(Math.random() * 800) + 10 : 0;

        remainingTimeout = timeout - subTimeoutOn - subTimeoutOff;

        this.crackle.play();
        setTimeout(function () {
            that.crackle.pause();
            if(remainingTimeout > 0){
                setTimeout(function(){
                    that.processing(remainingTimeout);
                },subTimeoutOff)
            }
        }, subTimeoutOn);
    },
    writeToDisk: function(timeout){
        var that = this,
            subTimeoutOn = timeout < 1000 ?
                timeout : timeout > 1200 ?
                    Math.round(Math.random() * 1000) + 100 :
                    Math.round(Math.random() * timeout) + 100,

            subTimeoutOff = timeout - subTimeoutOn > 0  ? Math.round(Math.random() * 500) + 10 : 0;

        remainingTimeout = timeout - subTimeoutOn - subTimeoutOff;

        this.disk.play();
        setTimeout(function () {
            that.disk.pause();
            if(remainingTimeout > 0){
                setTimeout(function(){
                    that.processing(remainingTimeout);
                },subTimeoutOff)
            }
        }, subTimeoutOn);
    },
    playBeeb: function(type){
        if(this.beebs[type] != undefined && !this.beebs[type].playing){
            this.beebs[type].load();
            this.beebs[type].play();
        }
    },
    tone: function (freq) {
        var oscillator = this.audioContext.createOscillator();
        this.oscillators.push(oscillator);
        oscillator.type = 0;
        oscillator.frequency.value = freq;
        gainNode = this.audioContext.createGain ? this.audioContext.createGain() : this.audioContext.createGainNode();
        oscillator.connect(gainNode,0,0);
        gainNode.connect(this.audioContext.destination);
        gainNode.gain.value = .1;
        oscillator.start ? oscillator.start() : oscillator.noteOn(0)
    },
    stopTones : function() {
        var self = this;
        setTimeout(function () {
            for(var i = 0; i < self.oscillators.length; i++){
                var oscillator = self.oscillators.pop();
                oscillator.disconnect();
            }
        },200);
    },
    playRepeated: function(freq, checkCallback, delay1, delay2){
        var self = this;
        delay2 = delay2 || delay1;

        setTimeout(function(){
            AudioCore.tone(freq);
            setTimeout(function(){
                AudioCore.stopTones();
                if(!checkCallback()){
                    self.playRepeated(freq, checkCallback, delay1, delay2);
                }
            }, delay2);
        }, delay1);
    },
    playBabble: function(index){
        if(index){
            this.babble[index].play();
        }else{
            faker.random.arrayElement(this.babble).play();
        }
    },
    bootdownAudio: function(){
        this.bootdown.play();
        this.idle.pause();
        this.crackle.pause();
        this.bootup.pause();
        this.bootup.load();
    }
}