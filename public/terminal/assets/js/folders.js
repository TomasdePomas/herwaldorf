/**
 * Created by Tomas on 17-1-2016.
 */
$window.on("window:folder:opened", function(event, options){

  var path = FS.getPath(options.path);

  if(path){
    var contents = FS.getByPath(path),
        $targetArea = $('[uuid="'+ options.uuid +'"] .inner-area');

    _.each(contents.content, function(item){
      var $item = $('<a/>');
      $item.text(item.name);
      $item.attr('class', 'icon black');
      $item.attr('type', item.type);
      $item.dblclick(function() {
        if(item.type == 'folder'){
          openWindow(item.type, { path: path + "/" + item.name, uuid: options.uuid, scalable: true, data: item })
        }else if(item.type == 'executable') {
          error();
        }else{
          openWindow(item.type, { path: path + "/" + item.name, data: item})
        }
      });
      $targetArea.append($item);
    });
  }

});