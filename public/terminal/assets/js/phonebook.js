/**
 * This code has been shamelessly ripped from http://onlinetonegenerator.com/dtmf.html
 * On: 7-3-16 - 20:11
 */

$window.on("window:phonebook:opened", function () {


});

searchPhonebook = function () {
    var resultContainer = $('.phonebook-searchresults');
    resultContainer.empty();
    var types = [
        "Residence",
        "Company",
        "Bar",
        "Company"];

    $adress = $("[name='phone-address']");
    $name = $("[name='phone-name']");


    if ($adress.val()) {

        $.get('/phonebook/by_address/' + $adress.val(), function (data) {
            var html = "<div>";
            if (data.length) {
                _.each(data, function(record){
                    if (record.name) {
                        html += record.name + "<br>";
                    }
                    html += "<strong>Number: " + record.phone_number_humanized + "</strong><br>";
                    html += "<p>Adress: " + record.address + "<br>";
                    html += "type: " + types[parseInt(record.type)] + "</p></div>";
                });
            }else{
                html += "No results</div>";
            }
            resultContainer.html(html);

        });

    } else {
        $.get('/phonebook/by_name/' + $name.val(), function (data) {
            var html = "<div>";
            if (data.length) {
                if (data[0].name) {
                    html += "<em>" + data[0].name + "</em><br>";
                }
                html += "<strong>Number: " + data[0].phone_number_humanized + "</strong><br>";
                html += "<p>Adress: " + data[0].address + "<br>";
                html += "type: " + types[parseInt(data[0].type)] + "</p></div>";

            }else{
                html += "No results</div>";
            }
            resultContainer.html(html);
        });


    }


}