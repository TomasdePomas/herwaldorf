/**
 * Created by Tomas on 2-12-2015.
 */
var FS = {
    noteContent: "<strong>NOTE</strong>",
    drive: 'C:',
    storage: {
        content: [
            {
                name: 'C:',
                type: 'drive',
                content: [
                    {
                        name: 'Documents',
                        type: 'folder',
                        content: [
                            {
                                name: 'Music',
                                type: 'folder',
                                content: []
                            },
                            {
                                name: 'Pictures',
                                type: 'folder',
                                content: [{
                                    name: 'image.jpg',
                                    type: 'image',
                                    src: '02'
                                }, {
                                    name: 'image2.jpg',
                                    type: 'image',
                                    src: '23'
                                }, {
                                    name: 'image3.jpg',
                                    type: 'image',
                                    src: '12'
                                }, {
                                    name: 'image4.jpg',
                                    type: 'image',
                                    src: '08'
                                }, {
                                    name: 'image5.jpg',
                                    type: 'image',
                                    src: '05'
                                }, {
                                    name: 'image6.jpg',
                                    type: 'image',
                                    src: '17'
                                }, {
                                    name: 'image7.jpg',
                                    type: 'image',
                                    src: '14'
                                }, {
                                    name: 'image8.jpg',
                                    type: 'image',
                                    src: '03'
                                }, {
                                    name: 'image9.jpg',
                                    type: 'image',
                                    src: '21'
                                }, {
                                    name: 'image10.jpg',
                                    type: 'image',
                                    src: '26'
                                }, {
                                    name: 'image11.jpg',
                                    type: 'image',
                                    src: '10'
                                }, {
                                    name: 'image12.jpg',
                                    type: 'image',
                                    src: '16'
                                }]
                            }
                        ]
                    },
                    {
                        name: 'Sys',
                        type: 'folder',
                        content: [
                            {
                                name: 'fs2m2k.sh',
                                type: 'executable'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'A:',
                type: 'drive',
                content: [
                    {
                        name: 'Backup',
                        type: 'folder',
                        content: [{
                            name: '12_02_1963',
                            type: 'folder',
                            content: [{
                                name: 'image.jpg',
                                type: 'image',
                                src: '13'
                            }, {
                                name: 'image2.jpg',
                                type: 'image',
                                src: '08'
                            }, {
                                name: 'image3.jpg',
                                type: 'image',
                                src: '16'
                            }]
                        }, {
                            name: '12_01_1963',
                            type: 'folder',
                            content: [{
                                name: 'image.jpg',
                                type: 'image',
                                src: '01'
                            }, {
                                name: 'image2.jpg',
                                type: 'image',
                                src: '16'
                            }, {
                                name: 'image3.jpg',
                                type: 'image',
                                src: '04'
                            }, {
                                name: 'image4.jpg',
                                type: 'image',
                                src: '09'
                            }, {
                                name: 'image5.jpg',
                                type: 'image',
                                src: '02'
                            }, {
                                name: 'image6.jpg',
                                type: 'image',
                                src: '19'
                            }, {
                                name: 'image9.jpg',
                                type: 'image',
                                src: '20'
                            }, {
                                name: 'image10.jpg',
                                type: 'image',
                                src: '28'
                            }]
                        }]
                    }
                ]
            }
        ]
    },
    setStartData: function (data) {
        FS.noteContent = "<p>Notes</p><br>";
        FS.noteContent += data.start_data.handler.metro_number + "<br><br>";
        FS.noteContent += data.start_data.handler.account_number + "<br><br><br>";
        _.each(data.start_data.meetings, function (meeting) {
            var date = new Date(meeting.date);
            FS.noteContent += "<p>" + date.toLocaleDateString() + ":  <strong>" + meeting.code + "</strong></p>";
        });
        FS.noteContent += "<br><br>";
        FS.noteContent += "<p>P.S. The librarian knows many things: " + data.start_data.librarian + "<br>";
    },
    switchDisk: function (letter) {
        var drive = _.findWhere(this.storage.content, {name: letter, type: "drive"});
        if (drive) {
            this.drive = drive.name;
            return true
        }
        return false;
    },
    getRootPath: function () {
        return this.drive;
    },
    getRootDir: function () {
        return _.findWhere(this.storage.content, {name: this.getRootPath()});
    },
    getByPath: function (path) {
        var bits = path.split('/'),
            fs = this,
            curDir = this.storage;

        _.each(bits, function (bit) {
            curDir = _.findWhere(curDir.content, {name: bit, type: "folder"});
            if (!curDir) {
                curDir = _.findWhere(fs.storage.content, {name: bit, type: "drive"});
                if (!curDir) {
                    return false;
                }
            }
        });
        return curDir;
    },
    getPath: function (path) {
        var bits = path.split('/');
        var url = [];

        _.each(bits, function (bit, i) {
            if (bit == '..' && url.length) {
                url.pop();
            } else if (bit != '..') {
                url.push(bit);
            }
        });
        return url.join('/');
    },
    getContents: function (path) {
        var dir = this.getByPath(path);
        if (dir) {
            return dir.content;
        }
        throw "The system cannot find the path specified.";
    }
};