/**
 * Created by Tomas on 3-12-2015.
 */

secondBoot = false;

function fastBoot() {
    fastMode = true;
    $('#turnoff-message').hide();
    $('.preface').hide();
    $('.blink').hide();
    $('.preloader').hide();
    $('#desktop').addClass('ready');
    $('#desktop').addClass('booted');
    $('.navbar, .container').show();
    $('a.icon').show();
}

function bootUp() {
    fastMode = false;
    var $preface = $('.preface');
    var lines = [
        faker.fake('{{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.adjective}} {{hacker.noun}}'),
        faker.fake('{{hacker.abbreviation}} {{hacker.noun}} {{hacker.ingverb}} the {{hacker.noun}}'),
        faker.fake('{{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.adjective}} {{hacker.noun}}'),
        faker.fake('{{hacker.ingverb}} {{hacker.noun}} for {{hacker.abbreviation}}'),
        faker.fake('{{hacker.abbreviation}} {{hacker.noun}} {{hacker.ingverb}} the {{hacker.adjective}} {{hacker.noun}}'),
        faker.fake('{{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.adjective}} {{hacker.verb}}')
    ];

    $preface.text('');


    setTimeout(function () {
        AudioCore.playBeeb('start');

        setTimeout(function () {

            $preface.text('');
            $preface.append('<br>> booting..');
            $('a.icon').hide();
            $('.blink').show();

            setTimeout(function () {
                AudioCore.processing(30000);
                $preface.append('<br>> ' + lines[0]);

                setTimeout(function () {
                    $preface.append('<br>> ' + lines[1]);

                    setTimeout(function () {
                        $preface.append('<br>> ' + lines[2]);

                        setTimeout(function () {
                            $preface.append('<br>> ..');

                            setTimeout(function () {
                                $preface.append('<br>> ..');

                                setTimeout(function () {
                                    $preface.append('<br>> ..');

                                    setTimeout(function () {
                                        $preface.append('<br>> complete<br>>');

                                        setTimeout(function () {
                                            $preface.append('<br>> ' + lines[3]);

                                            setTimeout(function () {
                                                $preface.append('<br>> ' + lines[4]);

                                                setTimeout(function () {
                                                    $preface.append('<br>>');
                                                    $preface.append('<br>>');
                                                    $preface.append('<br>> boot completed.. starting OS..');

                                                    setTimeout(function () {
                                                        $preface.hide();
                                                        $('.blink').hide();

                                                        setTimeout(function () {
                                                            $('#desktop').addClass('ready');
                                                            $('.preloader').show();
                                                            var progress = 0;

                                                            setInterval(function () {
                                                                progress = Math.floor(Math.random() * 6) + progress;
                                                                if (progress < 110) {
                                                                    $('#preloaderProgress').css('width', progress + "%");
                                                                }
                                                            }, 100);

                                                            setTimeout(function () {
                                                                $('#desktop').removeClass('ready');
                                                                $('#preloaderProgress').css('width', "100%");
                                                                $('.preloader').hide();

                                                                setTimeout(function () {
                                                                    if (secondBoot) {
                                                                        completeBoot();
                                                                    } else {
                                                                        $('.preinput').show();
                                                                    }
                                                                }, 3500);
                                                            }, 5000);
                                                        }, 2000);
                                                    }, 1200);
                                                }, 3000);
                                            }, 1800);
                                        }, 1200);
                                    }, 1600);
                                }, 1600);
                            }, 1600);
                        }, 2000);
                    }, 1800);
                }, 2200);
            }, 2200);
        }, 500);
    }, 7000);
}
function prepareData() {
    $('#prepareButton').remove();
    $('#prepareProgress').show();
    $progress = $('#prepareProgress .progress-bar');

    $input = $('#badgenumber');
    $input.attr('readonly', true);

    AudioCore.processing(20000);

    $.get('/generate_data/' + $input.val(), function (data) {
        if(!data.start_data){
            var progress = (1 + parseInt(data.step)) / 8 * 100;
            $progress.css('width', progress + "%");

            prepareData();
        }else{
            secondBoot = true;
            $('.preinput').hide();
            FS.setStartData(data);
            $('#desktop').addClass('ready');
            completeBoot();
        }
    });

}
function completeBoot() {
    $('#desktop').addClass('booted');

    setTimeout(function () {
        $('.navbar, .container').show();
        AudioCore.playWelcome();
        setTimeout(function () {
            $('a.icon').show();
        }, 3000);
    }, 1000);
}
function bootDown() {
    $('a.icon').hide();
    $('.preface').text('');

    closeAllWindows(1000);

    AudioCore.processing(14000);

    setTimeout(function () {
        $('.navbar, .container').hide();

        setTimeout(function () {
            $('#desktop').removeClass('ready');
            $('#desktop').removeClass('booted');


            setTimeout(function () {
                $('.blink').show();
                setTimeout(function () {
                    $('.blink').hide();
                    setTimeout(function () {
                        $('#display').css("background", "#000");
                        $('#turnoff-message').show();
                    }, 1200);
                }, 4500);
            }, 1000);
        }, 5000);
    }, 2000);
}

function error() {
    var runs = 0;
    var $preface = $('.preface');
    var $desktop = $('#desktop');

    closeAllWindows(false);

    $desktop.css("transition", "background-color 0s");
    $desktop.css("background", "#000084");
    $desktop.removeClass('ready');
    $desktop.removeClass('booted');

    $('a.icon').hide();
    $preface.text('');
    $preface.show();

    $('.navbar, .container').hide();

    AudioCore.playBeeb('error');
    AudioCore.processing(10000);
    setInterval(function () {
        if (runs < 200) {
            var lines = [
                faker.fake('>[ERROR] {{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.adjective}} {{hacker.noun}} at {{internet.ip}}'),
                faker.fake('>[FATAL] {{hacker.abbreviation}} {{hacker.noun}} {{hacker.ingverb}} the {{hacker.noun}} at line {{internet.mac}}'),
                faker.fake('>[ERROR] {{hacker.ingverb}} {{hacker.noun}} for {{hacker.abbreviation}} at line {{internet.color}} on {{random.number}}'),
                faker.fake('>[WARNING] {{hacker.abbreviation}} {{hacker.noun}} {{hacker.ingverb}} for {{hacker.abbreviation}} while running {{internet.color}} on {{random.number}}'),
                faker.fake('>[WARNING] {{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.abbreviation}} {{hacker.adjective}} {{hacker.verb}} at line {{random.number}}'),
                faker.fake('>[FATAL] {{random.uuid}}{{random.number}}{{internet.ip}}{{random.uuid}}'),
                faker.fake('>[FATAL] {{random.number}}{{random.uuid}}{{internet.ip}}'),
                faker.fake('>[ERROR] {{internet.mac}}{{internet.mac}}{{internet.ip}}'),
                faker.fake('>[ERROR] {{random.uuid}}{{random.uuid}}'),
                faker.fake('>[WARNING] {{random.number}}{{random.uuid}}{{internet.mac}}'),
                faker.fake('>[ERROR] {{hacker.ingverb}} the {{hacker.noun}} {{hacker.noun}} for {{hacker.adjective}} {{hacker.verb}} running {{hacker.abbreviation}} while {{hacker.ingverb}}}')
            ];

            $preface.append(faker.random.arrayElement(lines) + '<br>');
            $preface.scrollTop($preface[0].scrollHeight);
        }
    }, 60);
}

function switchOff() {
    var $preface = $('.preface');
    var $desktop = $('#desktop');

    $preface.show();
    $('.blink').show();

    $desktop.removeClass('ready');
    $desktop.removeClass('booted');
    $desktop.css("background", "");
    $desktop.css("transition", "background-color 0s");

    $('.navbar, .container').hide();

    var nrfTimeOuts = setTimeout(function () {
    }, 100);
    for (var i = 0; i < nrfTimeOuts; i++) {
        clearTimeout(i);
    }
}