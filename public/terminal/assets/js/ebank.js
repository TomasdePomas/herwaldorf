var bankCypher = false;

$window.on("window:ebank:opened", function(event, options){
    if(!bankCypher){
        $('[uuid="'+ options.uuid +'"]').remove();
        openWindow('ebank-lock', {multiple: false})
    }
});


$window.on("window:ebank-lock:opened", function(event, options){
    if(bankCypher){
        $('[uuid="'+ options.uuid +'"]').remove();
        openWindow('ebank', {multiple: false, cssClass: 'maximized bg-black'})
    }
});

$(document).on("click","[data-bank-increase]",function() {
    var $this = $(this),
        $target = $('[data-bank-digit="'+ $this.data('bank-increase') + '"]'),
        val = parseInt($target.text());

    val = val == 9 ? 0 : val + 1;

    $target.text(val);
});

$(document).on("click","[data-bank-decrease]",function() {
    var $this = $(this),
        $target = $('[data-bank-digit="'+ $this.data('bank-decrease') + '"]'),
        val = parseInt($target.text());

    val = val == 0 ? 9 : val - 1;

    $target.text(val);
});

$(document).on("click",".enterBankKey",function() {
    var $digits = $('[data-bank-digit]'),
        code = $digits.text().replace(/ /g, "");

    $.get("/bank/verify", { code : code }, function(data, status) {
        bankCypher = code;
        window.setTimeout(function(){
            $('#ebank-lock').remove();
            openWindow('ebank', {multiple: false, cssClass: 'maximized bg-black'})
        }, 500);

        $('[data-bank-digit="1"],[data-bank-digit="2"],[data-bank-digit="3"],[data-bank-digit="4"]').text(' ');
    }).error(function () {
        window.setTimeout(function(){
            $('[data-bank-digit="1"],[data-bank-digit="2"],[data-bank-digit="3"],[data-bank-digit="4"]').text('1');
        }, 800);

        $('[data-bank-digit="1"],[data-bank-digit="2"],[data-bank-digit="3"],[data-bank-digit="4"]').text('X');
    });
});

loadBankRecords = function(e){
    var resultContainer = $('.ebank-searchresults');
    resultContainer.empty();

    $number = $("[name='bankNumber']");

    if ($number.val()) {

        $.get('/bank/by_number/' + $number.val() + "?code="+ bankCypher, function (data) {
            var html = "";
            if (data.length) {
                _.each(data, function(record){
                    html += "<tr><td>"+ record.description + "</td>";

                    if(record.to_account == $number.val()){
                        html += "<td>From: "+ record.from_account + "</td><td>"+ (record.amount / 100) + "</td><td></td>";
                    }else if(record.to_account != null){
                        html += "<td>To: "+ record.to_account + "</td><td></td><td class='negative'>-"+ (record.amount / 100) + "</td>";
                    }else{
                        html += "<td> -- </td><td></td><td class='negative'>-"+ (record.amount / 100) + "</td>";
                    }
                    html += "</tr>";
                });
            }else{
                html += "<tr><td>Unknown bankaccount</td></tr>";
            }
            resultContainer.html(html);

        });

    }
};