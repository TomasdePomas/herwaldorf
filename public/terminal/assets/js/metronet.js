/**
 * Created by TomasDePomas.
 * Using: PhpStorm
 * On: 14-3-16 - 21:29
 */
metroCypher = true;

$window.on("window:metronet:opened", function(event, options){
    if(!metroCypher){
        $('[uuid="'+ options.uuid +'"]').remove();
        openWindow('metronet-lock', {multiple: false})
    }else{
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })

        getStationOverview();
    }
});


$window.on("window:metronet-lock:opened", function(event, options){
    if(metroCypher){
        $('[uuid="'+ options.uuid +'"]').remove();
        openWindow('metronet', {multiple: false})
    }
});

$(document).on("click","[data-increase]",function() {
    var $this = $(this),
        $target = $('[data-digit="'+ $this.data('increase') + '"]'),
        val = parseInt($target.text());

    val = val == 9 ? 0 : val + 1;

    $target.text(val);
});

$(document).on("click","[data-decrease]",function() {
    var $this = $(this),
        $target = $('[data-digit="'+ $this.data('decrease') + '"]'),
        val = parseInt($target.text());

    val = val == 0 ? 9 : val - 1;

    $target.text(val);
});

$(document).on("click",".enterKey",function() {
    var $digits = $('[data-digit]'),
        code = $digits.text().replace(/ /g, "");

    $.get("/metro/verify", { code : code }, function(data, status) {
        metroCypher = code;
        window.setTimeout(function(){
            $('#metronet-lock').remove();
            openWindow('metronet', {multiple: false})
        }, 500);

        $('[data-digit="1"],[data-digit="2"],[data-digit="3"],[data-digit="4"]').text(' ');
    }).error(function () {
        window.setTimeout(function(){
            $('[data-digit="1"],[data-digit="2"],[data-digit="3"],[data-digit="4"]').text('1');
        }, 800);

        $('[data-digit="1"],[data-digit="2"],[data-digit="3"],[data-digit="4"]').text('X');
    });
});

$('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
})

getStationOverview = function(e){
    var $container = $('.StationContainer');
    $container.empty();
    $.get("/metro/overview", function(data){
        var html = "";
        if(data.length){
            _.each(data, function(entry){
                html += "<tr><td>"+entry.name+"</td></tr>";
            });
        }else{
            html += "<tr><td>No results found </td></tr>";
        }
        $container.html(html);
    });
};
searchTravelsByCardnumber = function(e){
    var $container = $('.travelSearchContainer'),
        $cardNumberInput = $('[name="cardNumberInput"]');
    $container.empty();
    $.get("/metro/by_number/" + $cardNumberInput.val(), function(data){
        var html = "";
        console.log(data);
        console.log(data.length);
        if(data.length){
            _.each(data, function(entry){
                var date = new Date(entry.date);
                html += "<tr><td>" + date.toLocaleDateString() + "</td><td> from " + entry.origin.name + " to " + entry.destination.name + "</td></tr>";
            });
        }else{
            html += "<tr><td> No results found </td></tr>";
        }
        $container.html(html);
    });
};
searchTravelsByStation = function(e){
    var $container = $('.travelStationSearchContainer'),
        $stationNameInput = $('[name="metroNameSearchInput"]');
    $container.empty();
    $.get("/metro/by_station/" + $stationNameInput.val(), function(data){
        var html = "";
        if(data.length){
            _.each(data, function(entry){
                var date = new Date(entry.date);
                html += "<tr><td>" + date.toLocaleDateString() + "</td><td>" + entry.metro_number +  "</td><td> from " + entry.origin.name + " to " + entry.destination.name + "</td></tr>";
            });
        }else{
            html += "<tr><td> No results found </td></tr>";
        }
        $container.html(html);
    });
};
searchStationByAddress = function(e){
    var $container = $('.metroAddressSearchContainer'),
        $metroAddressInput = $('[name="metroAddressInput"]');
    $container.empty();
    $.get("/metro/by_address/" + $metroAddressInput.val(), function(data){
        var html = "";
        if(data.length){
            _.each(data, function(entry){
                html += "<tr><td>"+entry.name+"</td></tr>";
            });
        }else{
            html += "<tr><td> No results found </td></tr>";
        }
        $container.html(html);
    });
};
searchNearStation = function(e){
    var types = [
        "Residence",
        "Company",
        "Bar",
        "Company"];
    var $container = $('.nearMetroSearchContainer'),
        $metroNameInput = $('[name="metroNameInput"]');
    $container.empty();
    $.get("/metro/whats_near/" + $metroNameInput.val(), function(data){
        var html = "";
        if(data.length){
            _.each(data, function(entry){
                html +=  "<tr><td>"+types[entry.type] + " </td><td> " + entry.name + "</td></tr>";
            });
        }else{
            html += "<tr><td> No results found </td></tr>";
        }
        $container.html(html);
    });
};