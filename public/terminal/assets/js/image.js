/**
 * Created by TomasDePomas.
 * Using: PhpStorm
 * On: 13-3-16 - 16:02
 */

$window.on("window:image:opened", function(event, options){
    $this = $("[uuid='" + options.uuid + "']");
    $this.find('img').attr('src', "/terminal/assets/img/files/" + options.data.src);

    if(options.path){
        var parentPath = options.path.split('/');
        parentPath.pop();
        parentPath = parentPath.join('/');

        var parentDir = FS.getByPath(parentPath),
            files = _.where(parentDir.content, { type: 'image' });

        if(files.length){
            var index = _.pluck(files, 'name').indexOf(options.data.name);

            if(index == 0){
                $this.find('.clickBack').addClass('disabled');
                $this.find('.clickNext').click(function(){
                    openWindow('image', { path: parentPath + "/" + files[index + 1].name, uuid: options.uuid, scalable: true, data: files[index + 1] })
                });
            }else if(index == files.length - 1){
                $this.find('.clickNext').addClass('disabled');
                $this.find('.clickBack').click(function(){
                    openWindow('image', { path: parentPath + "/" + files[index - 1].name, uuid: options.uuid, scalable: true, data: files[index - 1] })
                });
            }
            else{
                $this.find('.clickBack').click(function(){
                    openWindow('image', { path: parentPath + "/" + files[index - 1].name, uuid: options.uuid, scalable: true, data: files[index - 1] })
                });
                $this.find('.clickNext').click(function(){
                    openWindow('image', { path: parentPath + "/" + files[index + 1].name, uuid: options.uuid, scalable: true, data: files[index + 1] })
                });
            }
        }else{
            $this.find('.clickBack, .clickNext').hide();
        }
    }else{
        $this.find('.clickBack, .clickNext').hide();
    }

});

