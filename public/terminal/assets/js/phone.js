/**
 * This code has been shamelessly ripped from http://onlinetonegenerator.com/dtmf.html
 * On: 7-3-16 - 20:11
 */


var tones = [697.0, 697.0, 697.0, 770.0, 770.0, 770.0, 852.0, 852.0, 852.0, 941.0];
var dailCanceled = false;

dailNumber= function(number){
    $("#numberDailed").append(number);
    AudioCore.tone(tones[number]);

    setTimeout(function(){
        AudioCore.stopTones()
    }, 1000);
};

placeCall = function(){
    AudioCore.tone(941.0);

    setTimeout(function(){
        AudioCore.stopTones();

        var number = $("#numberDailed").text().trim().replace(/[^0-9]/g, '');;
        var message = $("[name='phoneMessage']").val().trim();
        var i = Math.floor(Math.random() * 3) + 1;
        dailCanceled = false;

        AudioCore.playRepeated(440, function(){
            i--;
            if(i < 0){
                $.get("/phone/call/" + number + '/' + message, function(data){
                    $("#numberDailed").text(data.response);
                    AudioCore.playBabble(data.audio);
                }).fail(function() {
                    $("#numberDailed").text('');
                    var i = 10;

                    AudioCore.playRepeated(400, function(){
                        i--;
                        return (i < 0 || dailCanceled);
                    }, 400, 200);
                });
                return true;
            }
            return dailCanceled;
        }, 800, 1200);
    }, 200);
};

clearDailer = function(){
    AudioCore.tone(697.0);
    $("#numberDailed").text("");
    dailCanceled = true;

    setTimeout(function(){
        AudioCore.stopTones();
    }, 200);
};
toggleMessageContainer = function(){
    $('#phone_message_container').slideToggle()
}

$window.on("window:phone:closed", function() {
    dailCanceled = true;
});

