/**
 * Created by Tomas on 3-12-2015.
 */

function loadTemplate(name, target, data, callback) {
    var $target = typeof target == 'string' ? $(target) : target,
        bust = faker.random.uuid;


    $.get('/terminal/assets/js/templates/'+ name +'.html?' + bust, function (hbs) {
        var template = Handlebars.compile(hbs);

        $target.html(template(data));
        if(callback){
            callback();
        }
    }, 'html')
}

function capitalize(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}