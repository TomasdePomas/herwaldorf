/**
 * Created by Tomas on 7-12-2015.
 */
$window.on("window:terminal:opened", function(){
    var $terminal = $('.terminalBody'),
        greeting = faker.fake("{{commerce.product}} {{company.companySuffix}} version {{internet.ip}} \n" +
            "Copyright (C) 1963 {{commerce.productMaterial}} {{hacker.abbreviation}} {{company.bsNoun}} and the ");
    greeting += capitalize( faker.hacker.noun()) + " " + faker.company.companySuffix();

    var curDir = FS.getRootDir(),
        curPath = FS.getRootPath();

    $terminal.terminal(function (command, terminal)
    {
        var root = false;
        command = command.split(' ');
        if(command[0] == 'sudo'){
            root = true;
            command.shift();
        }


        switch (command[0].trim()){
            case '' :
                if(command.length > 1 && command.join(' ').trim() != '') {
                    terminal.echo(terminal.get_prompt() + "'" + command.join(' ') + "' is not recognized as an internal or external command operable program or batch file.");
                    break;
                }
                break;
            case 'cd' :
                var wait = Math.floor((Math.random() * 1000) + 500);
                AudioCore.processing(wait);
                terminal.pause();
                setTimeout(function () {
                    terminal.resume();
                    if (command[1].indexOf(":") != -1) {
                        if (FS.switchDisk(command[1])) {
                            terminal.set_prompt(command[1] + '> ');
                            curPath = command[1];
                            curDir = FS.getByPath(curPath);
                        }
                        else {
                            terminal.echo("The system cannot find the path specified.");
                        }
                    } else {
                        var dir = command[1].substring(command[1].length - 1) == "/" ? command[1].substring(0, command[1].length - 1) : command[1],
                            path = FS.getPath(curPath + "/" + dir);
                            curDir = FS.getByPath(path);

                        if (curDir && curDir.type == 'folder') {
                            curPath = path;
                            terminal.set_prompt(curPath + '> ');
                            terminal.prompt = curPath;
                        } else {
                            terminal.echo("The system cannot find the path specified.");
                        }
                    }
                }, wait);
                break;
            case 'ls' :
                var wait = Math.floor((Math.random() * 1000) + 500);
                AudioCore.processing(wait);
                terminal.pause();
                setTimeout(function () {
                    terminal.resume();
                    _.each(curDir.content, function(content){
                        if(content.type == "executable"){
                            terminal.echo("[[;#55FF55;]" + content.name + "]");
                        }else if(content.type == "folder"){
                            terminal.echo("[[;#EEEEEE;]" + content.name + "]");
                        }else{
                            terminal.echo("[[;#A45BEF;]" + content.name + "]");
                        }
                    });
                }, wait);
                break;

            case 'exit':
                var wait = Math.floor((Math.random() * 1000) + 500);
                AudioCore.processing(wait);
                terminal.pause();
                setTimeout(function () {
                    terminal.destroy();
                    $terminal.closest('.overlayWindow').remove();
                }, wait);
                break;

            case 'files':
                var wait = Math.floor((Math.random() * 1000) + 500);
                AudioCore.processing(wait);
                terminal.pause();
                setTimeout(function () {
                    terminal.resume();
                    openWindow('folder', { path: curPath, data: { name: curDir.name}});
                }, wait);
                break;
            case 'shutdown':
                var wait1 = Math.floor((Math.random() * 1000) + 500);
                var wait2 = Math.floor((Math.random() * 1000) + 500);
                AudioCore.processing(wait1);
                terminal.pause();

                setTimeout(function () {
                    terminal.destroy();
                    $terminal.closest('.overlayWindow').remove();
                    AudioCore.processing(wait2);
                    setTimeout(function () {
                        bootDown();
                    }, wait2);
                }, wait1);
                break;
            default:
                if(command.length == 1 && command[0].indexOf('./') == 0){
                    if(_.findWhere(curDir.content, {name : command[0].slice(2), type: 'executable'})){
                        var wait = Math.floor((Math.random() * 1000) + 500);
                        AudioCore.processing(wait);
                        terminal.pause();
                        setTimeout(function () {
                            terminal.resume();
                            Bash.execute(command[0].slice(2), command.slice(1), terminal, root);
                        }, wait);
                    }
                    else{
                        terminal.echo(terminal.get_prompt() + "'" + command[0] + "' is not recognized as an internal or external command\n operable program or batch file.");
                    }

                }else{
                    terminal.echo(terminal.get_prompt() + "'" + command[0] + "' is not recognized as an internal or external command\n operable program or batch file.");
                }
                break;
        }
    },{
        tabcompletion: true,
        greetings: greeting,
        height: 410,
        history: true,
        exit: false,
        prompt: curPath + '> ',
        onCommandChange: function(){
            $terminal.scrollTop($terminal[0].scrollHeight);
        },
        completion: function(terminal, arg, callback) {
            var command = terminal.get_command().split(' ');
            var root = false;

            if(command[0] == 'sudo'){
                command.shift();
                root = true;
            }

            if(command[0] == ""){
                return callback(['cd','ls','exit']);

            }else if(command[0] == 'cd' && command.length < 3) {
                var pathString = command[1] ? curPath + "/" + command[1] : curPath,
                    basePathString = pathString.substr(0, pathString.lastIndexOf("\/")),
                    cmdPath = basePathString.slice(curPath.length + 1),
                    path = FS.getPath(pathString),
                    contents = FS.getByPath(path);
                cmdPath = cmdPath ? cmdPath + '/' : '';

                if(!contents){
                    path = FS.getPath(basePathString);
                    contents = FS.getByPath(path);
                }

                var dirs = _.where(contents.content, { type: 'folder' }),
                    needle = command[1];

                if(needle && needle.indexOf('/') != 0){
                   needle = needle.slice(needle.lastIndexOf("\/") + 1);
                }

                if(dirs.length > 1){
                    if(needle){
                        var matches = [];

                        _.each(dirs, function(dir){
                            if(dir.name.indexOf(needle) == 0){
                                matches.push(dir.name);
                            }
                        });

                        if(matches.length > 1){
                            terminal.echo(terminal.get_prompt() + terminal.get_command());
                            _.each(matches, function(match){
                                terminal.echo(match);
                            });
                        }else if(matches.length){
                            if(root){
                                terminal.set_command("sudo cd " + cmdPath + matches[0] + "/");
                            }else{
                                terminal.set_command("cd " + cmdPath + matches[0] + "/");
                            }
                        }
                    }else{
                        terminal.echo(terminal.get_prompt() + terminal.get_command());
                        _.each(dirs, function(dir){
                            terminal.echo(dir.name);
                        });
                    }
                }else if(dirs.length){
                    if(needle){
                        if(dirs[0].name.indexOf(needle == 0)){
                            if(root){
                                terminal.set_command("sudo cd " + cmdPath + dirs[0].name + "/");
                            }else{
                                terminal.set_command("cd " + cmdPath + dirs[0].name + "/");
                            }
                        }
                    }else{
                        if(root) {
                            terminal.set_command("sudo cd " + cmdPath + dirs[0].name + "/");
                        }else{
                            terminal.set_command("cd " + cmdPath + dirs[0].name + "/");
                        }
                    }
                }
            }else if(command[0].indexOf('./') == 0 && command.length < 3) {
                var files = _.where(FS.getContents(curPath), {type : 'executable'}),
                    needle = command[0].slice(2);

                if(files.length > 1){

                    if(needle){
                        var matches = [];

                        _.each(files, function(file){
                            if(file.name.indexOf(needle) == 0){
                                matches.push(file.name);
                            }
                        });

                        if(matches.length > 1){
                            terminal.echo(terminal.get_prompt() + terminal.get_command());
                            _.each(matches, function(match){
                                terminal.echo(match);
                            });
                        }else if(matches.length){
                            if(root){
                                terminal.set_command("sudo ./" + matches[0]);
                            }else{
                                terminal.set_command("./" + matches[0]);
                            }
                        }

                    }
                    else{
                        terminal.echo(terminal.get_prompt() + terminal.get_command());
                        _.each(files, function(file){
                            terminal.echo(file.name);
                        });
                    }
                }else if(files.length && (!needle || files[0].name.indexOf(needle) != -1)){
                    if(root){
                        terminal.set_command("sudo ./" + files[0].name);
                    }else{
                        terminal.set_command("./" + files[0].name);
                    }

                }
            }
        }
    });
});