/**
 * Created by Tomas on 2-12-2015.
 */
var $window = $(window);
highestIndex = 10;

$(document).on("click",".windowBar .max",function() {
    var $container = $(this).closest('.overlayWindow');
    $container.addClass('maximized');
    $container.attr('oh', $container.height());
    $container.attr('ow', $container.width());
    $container.css('width', '');
    $container.css('height', '');
});
$(document).on("click",".windowBar .min" ,function() {
    var $container = $(this).closest('.overlayWindow');
    $container.removeClass('maximized');
    $container.attr('oh');
    $container.css('width', $container.attr('ow'));
    $container.css('height', $container.attr('oh'));
});
$(document).on("click",".windowBar .opt" ,function() {
    var $container = $(this).closest('.overlayWindow');
    $window.trigger("window:" + name + ":closed");
    $container.remove();
});
$(document).on("click", ".windowBar", function(){
    var $this = $(this);
    highestIndex++;
    $this.closest('.overlayWindow').css('z-index', highestIndex);
});

function openWindow(name, options){

    var $this = $(this),
        $desktop = $("#desktop"),
        wait1 =  !fastMode ? Math.floor((Math.random() * 2500) + 500) : 0,
        wait2 =  !fastMode ? Math.floor((Math.random() * 2500) + 500) : 0;

    options = _.extend({
        multiple: true,
        scalable: true,
        cssClass: ''
    }, options);

    if(options.multiple || !$("#" + name).length) {

        $desktop.addClass('loading');
        AudioCore.processing(wait1 + wait2);

        setTimeout(function () {
            var uuid = options.uuid,
                newWindow = false;

            if(!uuid){
                newWindow = true;
                uuid = faker.random.uuid();
                options.uuid = uuid;
                $("#desktop").prepend("<div id='" + name + "' class='overlayWindow " + options.cssClass + "' uuid='" + uuid + "'></div>");
            }

            name = name ? name : 'basicWindow';

            setTimeout(function (uuid) {
                $window = $("[uuid='" + uuid + "']");
                highestIndex++;
                $window.css('z-index', highestIndex);
                var data = options.data || null;
                loadTemplate(name, $window, data, function () {
                    $desktop.removeClass('loading');
                    $window.trigger("window:" + name + ":opened", options);

                    if(newWindow){
                        $window.draggable({
                            handle: ".windowBar",
                            containment: "#desktop",
                            scroll: false,
                            start: function(event) {
                                highestIndex++;
                                $(event.currentTarget).css('z-index', highestIndex);
                            }
                        });
                        if (options.scalable) {
                            $window.resizable({
                                helper: "ui-resizable-helper"
                            });
                        }
                    }
                });
            }, wait2, uuid);
        }, wait1);
    }else{
        highestIndex++;
        $("#" + name).css('z-index', highestIndex);
    }
}

function closeAllWindows(delay){
    if(delay){
        $('.overlayWindow').each(function(){
            var wait = Math.floor((Math.random() * delay) + delay/2),
                $this = $(this);

            setTimeout(function () {
                $this.remove();
            }, wait);
        });

    }else{
        $('.overlayWindow').remove();
    }
}