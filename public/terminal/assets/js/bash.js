/**
 * Created by TomasDePomas.
 * Using: PhpStorm
 * On: 13-3-16 - 22:11
 */


Bash = {
    crackle: 0,
    execute: function (script, args, terminal, root) {
        terminal.pause();
        if (!root) {
            terminal.echo("[[;red;]you have no access!]");
        } else {
            terminal.echo("Starting brute force attack on banking systems");
            Bash.checkCode(0, terminal);
        }
    },

    checkCode: function (code, terminal) {
        var codeString = "0000" + code;
        codeString = codeString.substr(codeString.length - 4);

        if (this.crackle < 0) {
            var wait = Math.floor((Math.random() * 5000) + 3000);
            AudioCore.processing(wait);
            this.crackle = wait / 50;
        }

        this.crackle--;

        terminal.echo("[Trying passcode: " + codeString + "]");

        setTimeout(function () {
            if ((code - 2) % 10 == 0) {
                $.get("/bank/verify", {code: code}, function (data) {
                    terminal.echo("[[;green;] - Force successful]");
                    terminal.resume();
                }).fail(function () {
                    terminal.echo("[[;yellow;] - Incorrect]");
                    Bash.checkCode((code + 1), terminal);
                });
            }else{
                terminal.echo("[[;yellow;] - Incorrect]");
                Bash.checkCode((code + 1), terminal);
            }
        }, 50);
    }
};