<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Wer ist der Waldorf?</title>
        <link href="terminal/assets/386/css/bootstrap.min.css" rel="stylesheet">
        <link href="terminal/assets/386/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="terminal/assets/css/crt.css" rel="stylesheet">
        <link href="terminal/assets/css/pc.css" rel="stylesheet">

        <script src="terminal/assets/js/lib/jquery-1.11.3.min.js" charset="utf-8"></script>
        <script src="terminal/assets/js/audiocore.js" charset="utf-8"></script>

        <style>
            ::-moz-selection {
                background: rgba(0,0,0,0);
            }

            ::selection {
                background: rgba(0,0,0,0);
            }
        </style>

    </head>
    <body>
        <input type="checkbox" id="switch">
        <label for="switch" class="switch-label"></label>

        <div id="frame" class="container">
            <iframe id="monitor" src="terminal/display.html" class="screen" width="768" height="488"></iframe>
            <div class="overlay">.</div>
        </div>

        <div id="computer">
        </div>

        <script>
            fastBoot = false;
            $(document).on("click","#switch:not(:checked)",function() {
                $('#monitor')[0].contentWindow.postMessage('off', '*');
            });
            $(document).on("click","#switch:checked",function() {
                if(fastBoot){
                    $('#monitor')[0].contentWindow.postMessage('fastboot', '*');
                }else {
                    $('#monitor')[0].contentWindow.postMessage('on', '*');
                }
            });
        </script>
    </body>
</html>