#!/usr/bin/env bash

sudo apt-get install php5-fpm php5 php5-cli php5-memcached memcached --assume-yes
sudo apt-get install libapache2-mod-php5
sudo a2enmod actions fastcgi alias rewrite
sudo service apache2 restart
sudo php5enmod xdebug