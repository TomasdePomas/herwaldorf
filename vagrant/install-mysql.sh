#!/usr/bin/env bash

sudo apt-get update --fix-missing
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password secret'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password secret'
sudo apt-get -y install mysql-server
sudo apt-get -y install mysql-client

mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'homestead'@'localhost' IDENTIFIED BY 'secret' WITH GRANT OPTION;" --password=secret

sudo apt-get -y install php5-mysql
sudo apt-get -y install php5-sqlite