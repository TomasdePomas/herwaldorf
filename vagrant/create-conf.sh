#!/usr/bin/env bash

mkdir /etc/apache2/ssl 2>/dev/null
openssl genrsa -out "/etc/apache2/ssl/$1.key" 1024 2>/dev/null
openssl req -new -key /etc/apache2/ssl/$1.key -out /etc/apache2/ssl/$1.csr -subj "/CN=$1/O=Vagrant/C=UK" 2>/dev/null
openssl x509 -req -days 365 -in /etc/apache2/ssl/$1.csr -signkey /etc/apache2/ssl/$1.key -out /etc/apache2/ssl/$1.crt 2>/dev/null

block="
<VirtualHost *:80>
    ServerAdmin tomas@cell-0.com
    ServerName $1

    DirectoryIndex index.html index.php
    DocumentRoot /home/vagrant/laravel/public
    <Directory />
        Options FollowSymLinks
        AllowOverride All
    </Directory>
    <Directory /home/vagrant/laravel/public>
        Options FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
        Require all granted
    </Directory>

    LogLevel warn
    ErrorLog /home/vagrant/laravel/storage/logs/apache-error.log
    CustomLog /home/vagrant/laravel/storage/logs/apache-acces.log combined
</VirtualHost>
"

echo "$block" > "/etc/apache2/sites-available/$1.conf"
ln -fs "/etc/apache2/sites-available/$1.conf" "/etc/apache2/sites-enabled/$1.conf"

cd laravel

composer dump-autoload
php artisan clear-compiled
php artisan cache:clear

cd ~

echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf
sudo a2enconf servername

service apache2 restart
service php5-fpm restart
